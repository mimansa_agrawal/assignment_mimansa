﻿using System;
using System.Collections.Generic;

namespace AssignmentModelApi.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        public string EmailAddress { get; set; }
        public string Dob { get; set; }
        public string Password { get; set; }

        public virtual Gender Gender { get; set; }
    }
}
