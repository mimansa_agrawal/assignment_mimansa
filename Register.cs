﻿using Assignment.BO;
using Assignment.Modal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assignment.dal
{
    public class Class1
    {
        private readonly AssignmentEntities _dbContext = new AssignmentEntities();
        public List<GenderBO> GetAllGenders()
        {
            var result = _dbContext.Genders.ToList();
            if (result.Any())
            {
                List<GenderBO> list = new List<GenderBO>();
                foreach (var data in result)
                {
                    GenderBO temp = new GenderBO()
                    {
                        GenderId = data.GenderId,
                        Name = data.Name,
                    };

                    list.Add(temp);
                }
                return list;
            }
            else
                return null;
        }
    }
}
