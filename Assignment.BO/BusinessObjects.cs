﻿using System;

namespace Assignment.BO
{
    public class GenderBO
    {
        public int GenderId { get; set; }
        public string Name { get; set; }

    }
    public class UserBO
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public string EmailAddress { get; set; }
        public string DOB { get; set; }
        public string Password { get; set; }

    }
    public class LoginBO
    {
        public string token { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public string Name { get; set; }
    }
    public class ForgetPwdBO
    {
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
        public string EmailAddress { get; set; }
    }
}
