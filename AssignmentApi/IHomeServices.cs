﻿using Assignment.BO;
using AssignmentModelApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssignmentApi
{
    public interface IHomeServices
    {
        // Task<IEnumerable<Gender>> ListAsync();
        //Task<ActionResult<Gender>> GetAllGenders();
        List<GenderBO> GetAllGenders();
        List<UserBO> GetAllUsers();
        LoginBO AuthenticateUser(LoginBO detail);
        int RegisterUser(UserBO detail);
    }
}
