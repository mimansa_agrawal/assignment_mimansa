﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Assignment.BO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace AssignmentApi.Controllers
{
 [Authorize]
    [ApiController]
    [Route("[controller]/[Action]")]
    public class HomeController : ControllerBase
    {
        private readonly IHomeServices _homeService;
        public HomeController(IHomeServices homeService)
        {
            _homeService = homeService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [EnableCors]
        [HttpGet]
        public ActionResult<IEnumerable<UserBO>> GetAllUsers()
        {
            var items = _homeService.GetAllUsers();
            if (items.Count == 0)
                return NotFound();
            return items;
        }
        [HttpGet]
        public ActionResult<IEnumerable<GenderBO>> GetAllGenders()
        {
            var items = _homeService.GetAllGenders();
            if (items.Count == 0)
                return NotFound();
            return items;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult<LoginBO> AuthenticateUser(LoginBO details)
        {
            var result = _homeService.AuthenticateUser(details);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<int> RegisterUser(UserBO details)
        {
            var result = _homeService.RegisterUser(details);
            return result;
        }
    }
}
