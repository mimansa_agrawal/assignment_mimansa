﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Assignment.BO;
using AssignmentModelApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace AssignmentApi
{/// <summary>
/// 
/// </summary>
    public class HomeServices : IHomeServices
    {
        private readonly AssignmentContext _context = new AssignmentContext();
        private readonly AppSettings _appSettings;
       
        public HomeServices(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<GenderBO> GetAllGenders()
        {
            using (var _dbContext = new AssignmentContext())
                return _dbContext.Gender.Select(a => new GenderBO() { GenderId = a.GenderId, Name = a.Name }).ToList();
        }
        public List<UserBO> GetAllUsers()
        {
            using (var _dbContext = new AssignmentContext())
            {
                var result = _dbContext.User.ToList();
                if (result.Any())
                {
                    List<UserBO> list = new List<UserBO>();
                    foreach (var data in result)
                    {
                        UserBO temp = new UserBO()
                        {
                            UserId = data.UserId,
                            FirstName = data.FirstName,
                            LastName = data.LastName,
                            DOB = data.Dob,
                            EmailAddress = data.EmailAddress,
                            Password = data.Password,
                            //GenderName = data.Gender.Name,
                            GenderId = data.GenderId
                        };
                        list.Add(temp);
                    }
                    return list;
                }
                else
                    return null;
            }
        }
        public LoginBO AuthenticateUser(LoginBO detail)
        {
            using (var context = new AssignmentContext())
            {
                var result = context.User.FirstOrDefault(x => x.EmailAddress == detail.EmailAddress && x.Password == detail.Password);
                var userDetail = new LoginBO();
                if (result != null)
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    //var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Secret));
                    var key = Encoding.UTF8.GetBytes(_appSettings.Secret);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                    new Claim(ClaimTypes.Name, result.UserId.ToString())
                        }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    IdentityModelEventSource.ShowPII = true;
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    userDetail.token = tokenHandler.WriteToken(token);
                    // remove password before returning
                    userDetail.Password = null;
                    userDetail.Name = result.FirstName;
                    return userDetail;
                }
            }
            return null;
        }

        public int RegisterUser(UserBO detail)
        {
            try
            {
                using (var _dbContext = new AssignmentContext())
                {
                    User Modal = new User();
                    Modal.FirstName = detail.FirstName;
                    Modal.LastName = detail.LastName;
                    Modal.GenderId = detail.GenderId;
                    Modal.EmailAddress = detail.EmailAddress;
                    Modal.Dob = detail.DOB;
                    Modal.Password = detail.Password;
                    _dbContext.User.Add(Modal);
                    _dbContext.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
