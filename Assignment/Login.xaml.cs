﻿using Assignment.ViewModal;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace Assignment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            //DataContext = new LoginViewModal();
            this.Loaded += Login_Loaded;
        }
        LoginViewModal loginViewModel = new LoginViewModal();
        private void RegisterClick(object sender, RoutedEventArgs e)
        {
            var registerWindow = new Register();
            registerWindow.Show();
            this.Close();
        }

        private void ForgotPwdClick(object sender, RoutedEventArgs e)
        {
            var chngPwd = new ForgetPwd();
            chngPwd.Show();
            this.Close();
        }
        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= Login_Loaded;
            Activate();
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (loginViewModel != null)
                loginViewModel.LoggedIn -= LoginVMLogin;
            loginViewModel = DataContext as LoginViewModal;

            if (loginViewModel == null) return;
            loginViewModel.LoggedIn += LoginVMLogin;
        }

        private void LoginVMLogin(string e)
        {
            if (loginViewModel.Loginauthenticated)
            {
                HomePage home = new HomePage();
                home.userName.Text = e;
                home.Show();
                Close();
            }
        }

    }
}
