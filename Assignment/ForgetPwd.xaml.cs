﻿using Assignment.ViewModal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Assignment
{
    /// <summary>
    /// Interaction logic for ForgetPwd.xaml
    /// </summary>
    public partial class ForgetPwd : Window
    {
        public ForgetPwd()
        {
            InitializeComponent();
            DataContext = new ForgotPwdViewModal();
        }
         private void BackNavigationClick(object sender, RoutedEventArgs e)
        {
            var mainWindow = new Login();
            mainWindow.Show();
            this.Close();
        }
    }
}
