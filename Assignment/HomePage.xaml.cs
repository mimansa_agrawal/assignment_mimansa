﻿using Assignment.ViewModal;
using System.Windows;


namespace Assignment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class HomePage : Window
    {
        public HomePage()
        {
            InitializeComponent();
            DataContext = new HomeViewModal();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Login mainWindow = new Login();
            mainWindow.Show();
            this.Close();
        }
    }
}
