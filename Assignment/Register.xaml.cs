﻿using Assignment.BO;
using Assignment.ViewModal;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Assignment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Register : Window
    {
        public ObservableCollection<GenderBO> studentInfo { set; get; }
        public GenderBO SelectedstudentInfo { set; get; }
        public Register()
        {
            InitializeComponent();
            DataContext = new RegisterViewModal();
            //bindCombo();
            ////this.DataContext = this;
            //cmbGender.ItemsSource = studentInfo;
        }
        public void bindCombo()
        {
            ObservableCollection<GenderBO> studentList = new ObservableCollection<GenderBO>();
            //studentList = bll.Register.GetAllGenders();

            studentInfo = studentList;

        }
        private void BackNavigationClick(object sender, RoutedEventArgs e)
        {
            var mainWindow = new HomePage();
            mainWindow.Show();
            this.Close();
        }

        private void CmbGender_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }
    }
}
