﻿using Assignment.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.bll
{
    public class Home
    {
        public static List<UserBO> GetAllUsers()
        {
            return dal.Home.GetAllUsers();
        }
        public static int GetAllUsersCount()
        {
            return dal.Home.GetAllUsersCount();
        }

    }
}
