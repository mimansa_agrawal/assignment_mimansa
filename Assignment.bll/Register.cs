﻿using Assignment.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.bll
{
    public class Register
    {
        public static List<GenderBO> GetAllGenders()
        {
            return dal.Register.GetAllGenders();
        }
        public static int RegisterUser(UserBO detail)
        {
            return dal.Register.RegisterUser(detail);
        }
    }
}
