﻿using Assignment.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.bll
{
    public class Login
    {
        public static LoginBO LoginUser(LoginBO detail)
        {
            return dal.Login.LoginUser(detail);
        }
        public static int UpdatePassword(ForgetPwdBO detail)
        {
            return dal.Login.UpdatePassword(detail);
        }
       
    }
}
