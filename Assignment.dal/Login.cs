﻿using Assignment.BO;
using Assignment.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.dal
{
    public class Login
    {
        /// <summary>
        /// authenticates user on login
        /// </summary>
        /// <param name="detail"></param>
        /// <returns></returns>
        public static LoginBO LoginUser(LoginBO detail)
        {
            using (var context = new AssignmentEntities())
            {
                var result = context.Users.FirstOrDefault(x => x.EmailAddress == detail.EmailAddress && x.Password == detail.Password);
                var userDetail = new LoginBO();
                if (result != null)
                {
                    userDetail.Name = result.FirstName;
                    return userDetail;
                }
            }
            return null;
        }
        /// <summary>
        /// updates user password
        /// </summary>
        /// <param name="detail"></param>
        /// <returns></returns>

        public static int UpdatePassword(ForgetPwdBO detail)
        {
            int res = 0;
            try
            {
                using (var context = new AssignmentEntities())
                {
                    var result = context.Users.FirstOrDefault(x => x.EmailAddress == detail.EmailAddress);
                    if (result == null) { return res; } //email address not exist
                    else
                    {
                        res = 1;
                        result.Password = detail.NewPassword;
                        context.SaveChanges();
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                res = 2;
                return res;
            }
        }
    }
}
