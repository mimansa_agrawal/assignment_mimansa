﻿using Assignment.BO;
using Assignment.Modal;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.dal
{
    public class Register
    {
        /// <summary>
        /// to get all genders
        /// </summary>
        /// <returns></returns>
        public static List<GenderBO> GetAllGenders()
        {
            var genderList = new List<GenderBO>();
            using (var _dbContext = new AssignmentEntities())
            {
                var list = _dbContext.spGetAllGenders().ToList();
                genderList = list.Select(item => new GenderBO
                {
                    GenderId = item.GenderId,
                    Name = item.Name
                }).ToList();
            }
            return genderList;
        }
        /// <summary>
        /// registers new user
        /// </summary>
        /// <param name="detail"></param>
        /// <returns></returns>
        public static int RegisterUser(UserBO detail)
        {
            var status = 0;
            try
            {
                using (var _dbContext = new AssignmentEntities())
                {
                    User Modal = new User();
                    Modal.FirstName = detail.FirstName;
                    Modal.LastName = detail.LastName;
                    Modal.GenderId = detail.GenderId;
                    Modal.EmailAddress = detail.EmailAddress;
                    Modal.DOB = detail.DOB;
                    Modal.Password = detail.Password;
                    _dbContext.Users.Add(Modal);
                    _dbContext.SaveChanges();

                    //var result = new ObjectParameter("Success", typeof(int));
                    //_dbContext.Proc_RegisterUser(detail.FirstName, detail.LastName, detail.GenderId, detail.EmailAddress, detail.DOB, detail.Password, result);
                    //Int32.TryParse(result.Value.ToString(), out status);
                }
                return 1;
            }
            catch (Exception ex)
            {
                return status;
            }
        }
    }
}
