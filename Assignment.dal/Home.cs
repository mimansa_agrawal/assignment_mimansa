﻿using Assignment.BO;
using Assignment.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.dal
{
    public class Home
    {
       /// <summary>
       /// get users list
       /// </summary>
       /// <returns></returns>
        public static List<UserBO> GetAllUsers()
        {
            using (var _dbContext = new AssignmentEntities())
            {
                var result = _dbContext.Users.ToList();
                if (result.Any())
                {
                    List<UserBO> list = new List<UserBO>();
                    foreach (var data in result)
                    {
                        UserBO temp = new UserBO()
                        {
                            UserId = data.UserId,
                            FirstName = data.FirstName,
                            LastName = data.LastName,
                            DOB = data.DOB,
                            EmailAddress = data.EmailAddress,
                            Password = data.Password,
                            GenderName = data.Gender.Name,
                            GenderId = data.GenderId
                        };
                        list.Add(temp);
                    }
                    return list;
                }
                else
                    return null;
            }

        }

        public static int GetAllUsersCount()
        {
            var result = 0;
            using (var _dbContext = new AssignmentEntities())
            {
                result = _dbContext.Users.Count();
            }
            return result;
        }
    }
}
