﻿using Assignment.BO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Assignment.ViewModal
{
    public class HomeViewModal : ApiViewModal ,INotifyPropertyChanged

    {
        private ICollectionView _dataGridCollection;
        private string _filterString;
        public ICollectionView DataGridCollection
        {
            get { return _dataGridCollection; }
            set { _dataGridCollection = value; NotifyPropertyChanged("DataGridCollection"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        //private UserBO _employee;
        private static List<UserBO> _employeeList;
        public static List<UserBO> employeeList
        {
            get
            {
                return _employeeList;
            }
            set
            {
                _employeeList = value;
            }
        }

        public HomeViewModal()
        {
            GetAllUsers();
            //GetAllUsersCount();
        }
        public void GetAllUsers()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:51765/");
            //client.DefaultRequestHeaders.Add("appkey", "myapp_key");
            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenBO.Token);
            HttpResponseMessage response = client.GetAsync("Home/GetAllUsers").Result;
            if (response.IsSuccessStatusCode)
            {
                employeeList = response.Content.ReadAsAsync<List<UserBO>>().Result;
            }
            //employeeList = bll.Home.GetAllUsers();
            DataGridCollection = CollectionViewSource.GetDefaultView(employeeList);
            DataGridCollection.Filter = new Predicate<object>(Filter);
        }
        public int GetAllUsersCount()
        {
            var count = bll.Home.GetAllUsersCount();
            return count;
        }

        public string FilterString
        {
            get { return _filterString; }
            set
            {
                _filterString = value;
                NotifyPropertyChanged("FilterString");
                FilterCollection();
            }
        }

        private void FilterCollection()
        {
            if (_dataGridCollection != null)
            {
                _dataGridCollection.Refresh();
            }
        }

        public bool Filter(object obj)
        {
            var data = obj as UserBO;
            if (data != null)
            {
                if (!string.IsNullOrEmpty(_filterString))
                {
                    return data.FirstName.ToLower().Contains(_filterString.ToLower()) || data.LastName.ToLower().Contains(_filterString.ToLower());
                }
                return true;
            }
            return false;
        }
    }
}
