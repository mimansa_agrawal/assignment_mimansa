﻿using Assignment.BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static Assignment.ViewModal.CommonViewModal;

namespace Assignment.ViewModal
{
    public class ForgotPwdViewModal : INotifyPropertyChanged
    {
        public string email { get; set; }
        public string newPwd { get; set; }
        private string _errorMessage;
        public string errorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                NotifyPropertyChanged("errorMessage");
            }
        }
        private ICommand _clickCommand;
        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new CommandHandler(() => ChangePassword(), () => CanExecute));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public bool CanExecute
        {
            get
            {
                // check if executing is allowed, i.e., validate, check if a process is running, etc. 
                return true;
            }
        }
        public void ChangePassword()
        {
            if (string.IsNullOrEmpty(email))
                errorMessage = "Please enter email.";
            else if (string.IsNullOrEmpty(newPwd))
                errorMessage = "Please enter new password.";
            else
            {
                var userDetail = new ForgetPwdBO();
                userDetail.EmailAddress = email;
                userDetail.NewPassword = newPwd;
                var result = bll.Login.UpdatePassword(userDetail);
                if (result == 0)
                {
                    errorMessage = "Email address not found";
                }
                if (result == 1)
                {
                    Clear();
                    errorMessage = "Password updated successfully.";
                }
                if (result == 2)
                {
                    Clear();
                    errorMessage = "Some error occured.";
                }
            }
        }
        public void Clear()
        {
            email = string.Empty;
            newPwd = string.Empty;
        }
    }
}
