﻿namespace Assignment.ViewModal
{
    public class ApiViewModal
    {
        public static class TokenBO
        {
            public static string Token { get; set; }
            public static string Password { get; set; }
            public static string EmailAddress { get; set; }
        }
    }
}
