﻿using Assignment.BO;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Input;
using static Assignment.ViewModal.CommonViewModal;

namespace Assignment.ViewModal
{
    public class LoginViewModal : ApiViewModal, INotifyPropertyChanged
    {
        public delegate void LoginEventHandler(string e);
        public event LoginEventHandler LoggedIn;
       
        public void OnLoggedIn(LoginBO result)
        {
            TokenBO.Token = result.token;
            if (LoggedIn != null) LoggedIn(result.Name);
        }
        //if (_loginViewModel != null)
        //      _loginViewModel.LoggedIn -= LoginVmLogin;
        //  _loginViewModel = DataContext as LoginVM;

        //  if (_loginViewModel == null) return;
        //  _loginViewModel.LoggedIn += LoginVmLogin;
        private string _password { get; set; }
        public string username { get; set; }
        public string password
        {
            get { return _password; }
            set
            {
                _password = value;
                //if (String.IsNullOrEmpty(value))
                //{
                //    throw new ApplicationException("Password is mandatory.");
                //}
            }
        }
        private string _errorMessage;
        public string errorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                NotifyPropertyChanged("errorMessage");


            }
        }
        private string _userErrorMessage;
        public string userErrorMessage
        {
            get
            {
                return _userErrorMessage;
            }
            set
            {
                _userErrorMessage = value;
                NotifyPropertyChanged("userErrorMessage");


            }
        }
        private string _pwdErrorMessage;
        public string pwdErrorMessage
        {
            get
            {
                return _pwdErrorMessage;
            }
            set
            {
                _pwdErrorMessage = value;
                NotifyPropertyChanged("pwdErrorMessage");
            }
        }
        private bool loginauthenticated = false;
        public bool Loginauthenticated
        {
            get { return loginauthenticated; }
            set
            {
                loginauthenticated = value;
                NotifyPropertyChanged("Loginauthenticated");
            }
        }
        private ICommand _clickCommand;
        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new CommandHandler(() => LoginUser(), () => CanExecute));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public bool CanExecute
        {
            get
            {
                // check if executing is allowed, i.e., validate, check if a process is running, etc. 
                return true;
            }
        }
        public void LoginUser()
        {
            //EmployeeList = new ObservableCollection<UserBO>(bll.Home.GetAllUsers());
            if (string.IsNullOrEmpty(username))
            {
                pwdErrorMessage = "";
                userErrorMessage = "Please enter username.";
            }
             

            else if (string.IsNullOrEmpty(password))
            {
                pwdErrorMessage = "Please enter password.";
                userErrorMessage = "";
                //throw new ApplicationException("Password is mandatory.");
            }
            else
            {
                var userDetail = new LoginBO();
                userDetail.EmailAddress = username;
                userDetail.Password = password;
                var jsonString = JsonConvert.SerializeObject(userDetail);
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:51765/");
                //client.DefaultRequestHeaders.Add("appkey", "myapp_key");
                client.DefaultRequestHeaders.Accept.Add(
                   new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsync("Home/AuthenticateUser", new StringContent(jsonString, Encoding.UTF8, "application/json")).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<LoginBO>().Result;
                    if (result != null)
                    {
                        Loginauthenticated = true;
                        OnLoggedIn(result);
                        errorMessage = "Login Successfull";
                    }
                    else
                    {
                        Loginauthenticated = false;
                        errorMessage = "Username or Password is incorrect";
                    }
                }
            }
        }
    }
}
