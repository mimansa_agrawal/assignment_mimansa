﻿using Assignment.BO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using static Assignment.ViewModal.CommonViewModal;

namespace Assignment.ViewModal
{
    public class RegisterViewModal : INotifyPropertyChanged
    {
        private GenderBO _gender;
        private ObservableCollection<GenderBO> _GenderList;
        public ObservableCollection<GenderBO> GenderList
        {
            get
            {
                return _GenderList;
            }
            set
            {
                _GenderList = value;
            }
        }
        public GenderBO gender
        {
            get { return _gender; }
            set
            {
                _gender = value;
            }
        }
        public RegisterViewModal()
        {
            GetAllGenders();
        }

        //public ObservableCollection<GenderBO> studentInfo { set; get; }
        //public GenderBO SelectedstudentInfo { set; get; }
        private string _firstname { get; set; }
        public string firstname
        {
            get { return _firstname; }
            set
            {
                _firstname = value;
                NotifyPropertyChanged("firstname");
            }
        }
        private string _errorMessage;
        public string errorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                NotifyPropertyChanged("errorMessage");
            }
        }
        private string _pwdErrorMessage;
        public string pwdErrorMessage
        {
            get
            {
                return _pwdErrorMessage;
            }
            set
            {
                _pwdErrorMessage = value;
                NotifyPropertyChanged("pwdErrorMessage");
            }
        }
        private string _dobErrorMessage;
        public string dobErrorMessage
        {
            get
            {
                return _dobErrorMessage;
            }
            set
            {
                _dobErrorMessage = value;
                NotifyPropertyChanged("dobErrorMessage");
            }
        }
        private string _fnameErrorMessage;
        public string fnameErrorMessage
        {
            get
            {
                return _fnameErrorMessage;
            }
            set
            {
                _fnameErrorMessage = value;
                NotifyPropertyChanged("fnameErrorMessage");
            }
        }
        private string _lnameErrorMessage;
        public string lnameErrorMessage
        {
            get
            {
                return _lnameErrorMessage;
            }
            set
            {
                _lnameErrorMessage = value;
                NotifyPropertyChanged("lnameErrorMessage");
            }
        }
        private string _genderErrorMessage;
        public string genderErrorMessage
        {
            get
            {
                return _genderErrorMessage;
            }
            set
            {
                _genderErrorMessage = value;
                NotifyPropertyChanged("genderErrorMessage");
            }
        }
        private string _emailErrorMessage;
        public string emailErrorMessage
        {
            get
            {
                return _emailErrorMessage;
            }
            set
            {
                _emailErrorMessage = value;
                NotifyPropertyChanged("emailErrorMessage");
            }
        }
        private string _lastname { get; set; }
        public string lastname
        {
            get { return _lastname; }
            set
            {
                _lastname = value;
                NotifyPropertyChanged("lastname");
            }
        }
        private string _password { get; set; }
        public string password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyPropertyChanged("password");
            }
        }
        private string _dob { get; set; }
        public string dob
        {
            get { return _dob; }
            set
            {
                _dob = value;
                NotifyPropertyChanged("dob");
            }
        }
        private string _email { get; set; }
        public string email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyPropertyChanged("email");
            }
        }
        private ICommand _clickCommand;

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ICommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new CommandHandler(() => RegisterUser(), () => CanExecute));
            }
        }
        public bool CanExecute
        {
            get
            {
                // check if executing is allowed, i.e., validate, check if a process is running, etc. 
                return true;
            }
        }
        public void RegisterUser()
        {
            if (string.IsNullOrEmpty(firstname))
            {
                fnameErrorMessage = "Please enter firstname.";
                lnameErrorMessage = ""; dobErrorMessage = ""; genderErrorMessage = ""; pwdErrorMessage = ""; emailErrorMessage = "";
            }

            else if (string.IsNullOrEmpty(lastname))
            {
                lnameErrorMessage = "Please enter lastname.";
                fnameErrorMessage = "";
                dobErrorMessage = ""; genderErrorMessage = ""; pwdErrorMessage = ""; emailErrorMessage = "";
            }

            else if (string.IsNullOrEmpty(dob))
            {
                dobErrorMessage = "Please enter dob.";
                fnameErrorMessage = "";
                lnameErrorMessage = ""; genderErrorMessage = ""; pwdErrorMessage = ""; emailErrorMessage = "";
            }

            else if (gender == null)
            {
                genderErrorMessage = "Please enter gender.";
                fnameErrorMessage = "";
                lnameErrorMessage = ""; dobErrorMessage = ""; pwdErrorMessage = ""; emailErrorMessage = "";
            }

            else if (string.IsNullOrEmpty(password))
            {
                pwdErrorMessage = "Please enter password.";
                fnameErrorMessage = "";
                lnameErrorMessage = ""; genderErrorMessage = ""; dobErrorMessage = ""; emailErrorMessage = "";
            }

            else if (string.IsNullOrEmpty(email))
            {
                emailErrorMessage = "Please enter email.";
                fnameErrorMessage = "";
                lnameErrorMessage = ""; genderErrorMessage = ""; pwdErrorMessage = ""; dobErrorMessage = "";
            }

            else
            {
                bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (password.Length < 8 || password.Length > 15)
                {
                    pwdErrorMessage = "Password length is min: 8 and max: 15.";
                    emailErrorMessage = "";
                }
                else if (!isEmail)
                {
                    emailErrorMessage = "Please enter valid mail.";
                    pwdErrorMessage = "";
                }

                else
                {
                    var userDetail = new UserBO();
                    userDetail.EmailAddress = email;
                    userDetail.Password = password;
                    userDetail.FirstName = firstname;
                    userDetail.LastName = lastname;
                    userDetail.DOB = dob;
                    userDetail.GenderId = gender.GenderId;
                    //var jsonString = JsonConvert.SerializeObject(userDetail);
                    //HttpClient client = new HttpClient();
                    //client.BaseAddress = new Uri("http://localhost:51765/");
                    ////client.DefaultRequestHeaders.Add("appkey", "myapp_key");
                    //client.DefaultRequestHeaders.Accept.Add(
                    //   new MediaTypeWithQualityHeaderValue("application/json"));
                    //HttpResponseMessage response = client.PostAsync("Home/RegisterUser", new StringContent(jsonString, Encoding.UTF8, "application/json")).Result;
                    //if (response.IsSuccessStatusCode)
                    //{
                    //    var result = response.Content.ReadAsAsync<int>().Result;
                    //    if (result == 1)
                    //    {
                    //        Clear();
                    //        errorMessage = "Registration successful.";
                    //    }
                    //    else
                    //    {
                    //        Clear();
                    //        errorMessage = "Some error occured.";
                    //    }
                    //}
                    var result = bll.Register.RegisterUser(userDetail);
                    if (result == 1)
                    {
                        Clear();
                        errorMessage = "Registration successful.";
                    }
                    else
                    {
                        Clear();
                        errorMessage = "Some error occured.";
                    }
                }
            }
        }
        public void Clear()
        {
            firstname = string.Empty;
            lastname = string.Empty;
            password = string.Empty;
            dob = string.Empty;
            email = string.Empty;
            //employee = null;
        }

        public void GetAllGenders()
        {
            //GenderList = new ObservableCollection<GenderBO>(bll.Register.GetAllGenders());
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:51765/");
                //client.DefaultRequestHeaders.Add("appkey", "myapp_key");
                client.DefaultRequestHeaders.Accept.Add(
                   new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("Home/GetAllGenders").Result;
                if (response.IsSuccessStatusCode)
                {
                    var genders = response.Content.ReadAsAsync<IEnumerable<GenderBO>>().Result;
                    GenderList = new ObservableCollection<GenderBO>(genders);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
